# quantile-nc-prediction

Predicting Latency Quantiles using Network Calculus-assisted GNNs

## Training
```bash
sh compare-models-cross.sh
```

## Evaluation
```bash
sh eval-cross.sh
```
