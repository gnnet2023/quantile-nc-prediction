from .ggnn import GGNN
from .resggnn import ResGGNN

implemented_models = [GGNN, ResGGNN]
