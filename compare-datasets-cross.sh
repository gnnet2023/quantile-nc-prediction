python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_0/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_0/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_0/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_0/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_0/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_0/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_0/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_0/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_0/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_0/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_0/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_0/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_1/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_1/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_1/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_1/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_1/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_1/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_1/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_1/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_1/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_1/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_1/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_1/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_2/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_2/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_2/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_2/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_2/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_2/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_2/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_2/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_2/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_2/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_2/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_2/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_3/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_3/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_3/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_3/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_3/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_3/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_3/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_3/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_3/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_3/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_3/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_3/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_4/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_4/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_4/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_4/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_4/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_4/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_4/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_4/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_4/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_4/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_4/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_4/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_5/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_5/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_5/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_5/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_5/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_5/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_5/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_5/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_5/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_5/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_5/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_5/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_6/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_6/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_6/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_6/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_6/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_6/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_6/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_6/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_6/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_6/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_6/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_6/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_7/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_7/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_7/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_7/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_7/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_7/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_7/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_7/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_7/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_7/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_7/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_7/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_8/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_8/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_8/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_8/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_8/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_8/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_8/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_8/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_8/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_8/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_8/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_8/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_9/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_9/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_9/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_9/  --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_9/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_9/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait

python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/mean_nc_9/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/min_nc_9/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/max_nc_9/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/median_nc_9/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p90_nc_9/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &
python3 neural_network.py --train-test-split 0.8 --regression --model-architecture GraphSAGE --epochs 300 --batch-size 8 --dropout 0.3 --dropout-gru 0.1 --hidden-size 4 --jk --nunroll 4 --dataset datasets/p99_nc_9/ --model-output-dir models-comp/ --lr-scheduler-factor 0.7 &

wait
